package grafika.subdivision;

import javax.media.j3d.Appearance;
import javax.media.j3d.Shape3D;
import javax.media.j3d.TriangleArray;
import javax.vecmath.Point3d;

public class ShapeFactory {
	
	public Shape3D getCube(){
		Shape3D result = null;
		TriangleArray array = new TriangleArray(36, TriangleArray.COORDINATES + TriangleArray.COLOR_3);
		array.setCoordinate(0, new Point3d(-1.0, -1.0, 1.0));
		array.setCoordinate(1, new Point3d(1.0, 1.0, 1.0));
		array.setCoordinate(2, new Point3d(-1.0, 1.0, 1.0));
		array.setCoordinate(3, new Point3d(-1.0, -1.0, 1.0));
		array.setCoordinate(4, new Point3d(1.0, -1.0, 1.0));
		array.setCoordinate(5, new Point3d(1.0, 1.0, 1.0));
		array.setCoordinate(6, new Point3d(-1.0, 1.0, 1.0));
		array.setCoordinate(7, new Point3d(1.0, 1.0, -1.0));
		array.setCoordinate(8, new Point3d(-1.0, 1.0, -1.0));
		array.setCoordinate(9, new Point3d(-1.0, 1.0, 1.0));
		array.setCoordinate(10, new Point3d(1.0, 1.0, 1.0));
		array.setCoordinate(11, new Point3d(1.0, 1.0, -1.0));
		array.setCoordinate(12, new Point3d(-1.0, 1.0, -1.0));
		array.setCoordinate(13, new Point3d(1.0, 1.0, -1.0));
		array.setCoordinate(14, new Point3d(-1.0, -1.0, -1.0));
		array.setCoordinate(15, new Point3d(-1.0, -1.0, -1.0));
		array.setCoordinate(16, new Point3d(1.0, 1.0, -1.0));
		array.setCoordinate(17, new Point3d(1.0, -1.0, -1.0));
		array.setCoordinate(18, new Point3d(-1.0, -1.0, 1.0));
		array.setCoordinate(19, new Point3d(-1.0, -1.0, -1.0));
		array.setCoordinate(20, new Point3d(1.0, -1.0, -1.0));
		array.setCoordinate(21, new Point3d(-1.0, -1.0, 1.0));
		array.setCoordinate(22, new Point3d(1.0, -1.0, -1.0));
		array.setCoordinate(23, new Point3d(1.0, -1.0, 1.0));
		array.setCoordinate(24, new Point3d(-1.0, -1.0, 1.0));
		array.setCoordinate(25, new Point3d(-1.0, 1.0, 1.0));
		array.setCoordinate(26, new Point3d(-1.0, -1.0, -1.0));
		array.setCoordinate(27, new Point3d(-1.0, 1.0, 1.0));
		array.setCoordinate(28, new Point3d(-1.0, 1.0, -1.0));
		array.setCoordinate(29, new Point3d(-1.0, -1.0, -1.0));
		array.setCoordinate(30, new Point3d(1.0, -1.0, -1.0));
		array.setCoordinate(31, new Point3d(1.0, 1.0, -1.0));
		array.setCoordinate(32, new Point3d(1.0, 1.0, 1.0));
		array.setCoordinate(33, new Point3d(1.0, 1.0, 1.0));
		array.setCoordinate(34, new Point3d(1.0, -1.0, 1.0));
		array.setCoordinate(35, new Point3d(1.0, -1.0, -1.0));
		Appearance app = new Appearance();
		GeometryCollorer.colorGeometry(array);
		result = new Shape3D(array, app);
		return result;
	}
	
	public Shape3D getSquare() {
		Shape3D result = null;
		TriangleArray array = new TriangleArray(12, TriangleArray.COORDINATES + TriangleArray.COLOR_3);
		array.setCoordinate(0, new Point3d(1.0, 1.0, 0.0));
		array.setCoordinate(1, new Point3d(-1.0, 1.0, 0.0));
		array.setCoordinate(2, new Point3d(0.0, 0.0, 0.0));
		array.setCoordinate(3, new Point3d(-1.0, 1.0, 0.0));
		array.setCoordinate(4, new Point3d(-1.0, -1.0, 0.0));
		array.setCoordinate(5, new Point3d(0.0, 0.0, 0.0));
		array.setCoordinate(6, new Point3d(-1.0, -1.0, 0.0));
		array.setCoordinate(7, new Point3d(1.0, -1.0, 0.0));
		array.setCoordinate(8, new Point3d(0.0, 0.0, 0.0));
		array.setCoordinate(9, new Point3d(1.0, -1.0, 0.0));
		array.setCoordinate(10, new Point3d(1.0, 1.0, 0.0));
		array.setCoordinate(11, new Point3d(0.0, 0.0, 0.0));
		Appearance app = new Appearance();
		GeometryCollorer.colorGeometry(array);
		result = new Shape3D(array, app);
		return result;
	}
	
	public Shape3D getTriangle(){
		Shape3D result = null;
		TriangleArray array = new TriangleArray(3, TriangleArray.COORDINATES + TriangleArray.COLOR_3);
		array.setCoordinate(0, new Point3d(1.0, 0.0, 0.0));
		array.setCoordinate(1, new Point3d(0.0, 1.0, 0.0));
		array.setCoordinate(2, new Point3d(-1.0, 0.0, 0.0));
		Appearance app = new Appearance();
//		ColoringAttributes ca = new ColoringAttributes(this.colors.get(this.rand.nextInt(this.colors.size())), ColoringAttributes.NICEST);
//		app.setColoringAttributes(ca);
		GeometryCollorer.colorGeometry(array);
		result = new Shape3D(array, app);
		return result;
	}
	
	public Shape3D getTetrahedron(/*Point3d location*/){
		Shape3D result = null;
		int pointCount = 12;
		TriangleArray array = new TriangleArray(pointCount, TriangleArray.COORDINATES + TriangleArray.COLOR_3);
		array.setCoordinate(0, new Point3d(1.0, 0.0, 0.0));
		array.setCoordinate(1, new Point3d(0.0, 0.0, 1.0));
		array.setCoordinate(2, new Point3d(-1.0, 0.0, 0.0));
		array.setCoordinate(3, new Point3d(1.0, 0.0, 0.0));
		array.setCoordinate(4, new Point3d(0.0, 1.0, 0.5));
		array.setCoordinate(5, new Point3d(0.0, 0.0, 1.0));
		array.setCoordinate(6, new Point3d(1.0, 0.0, 0.0));
		array.setCoordinate(7, new Point3d(-1.0, 0.0, 0.0));
		array.setCoordinate(8, new Point3d(0.0, 1.0, 0.5));
		array.setCoordinate(9, new Point3d(0.0, 0.0, 1.0));
		array.setCoordinate(10, new Point3d(0.0, 1.0, 0.5));
		array.setCoordinate(11, new Point3d(-1.0, 0.0, 0.0));
		Appearance app = new Appearance();
		GeometryCollorer.colorGeometry(array);
		result = new Shape3D(array, app);
		return result;
	}

}
