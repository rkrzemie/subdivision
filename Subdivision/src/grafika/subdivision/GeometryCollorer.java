package grafika.subdivision;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.media.j3d.TriangleArray;
import javax.vecmath.Color3f;

public class GeometryCollorer {
	
	public static void colorGeometry(TriangleArray geometry){
		Random rand = new Random();
		int pointCount = geometry.getVertexCount();
		for(int i = 0 ; i < pointCount ; i += 3){
			Color3f color = new Color3f(rand.nextFloat(),rand.nextFloat(),rand.nextFloat());
			geometry.setColor(i, color);
			geometry.setColor(i+1, color);
			geometry.setColor(i+2, color);
		}
	}

}
