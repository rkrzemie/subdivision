package grafika.subdivision;

import java.util.LinkedList;
import java.util.List;

import javax.media.j3d.Appearance;
import javax.media.j3d.Shape3D;
import javax.media.j3d.TriangleArray;
import javax.vecmath.Point3d;

public class Subdiviser {
	
	private SubdivisionMethod method;
	
	public Subdiviser(){
		this.method = SubdivisionMethod.ROOT_3;
	}
	
	public Subdiviser(SubdivisionMethod method){
		this.method = method;
	}
	
	public Shape3D subdivision(Shape3D shape, int iterations){
		TriangleArray tmp = (TriangleArray)shape.getGeometry();
		TriangleArray newTmp = new TriangleArray(tmp.getVertexCount(),TriangleArray.COORDINATES + TriangleArray.COLOR_3);
		for(int i = 0 ; i < tmp.getVertexCount() ; i++){
			Point3d p = new Point3d(0.0,0.0,0.0);
			tmp.getCoordinate(i, p);
			newTmp.setCoordinate(i, p);
		}
		Appearance app = new Appearance();
		Shape3D result = new Shape3D(newTmp,app);
		for(int i = 0 ; i < iterations ; i++){
			switch(this.method){
				case ROOT_3:
					result = subdivisionRoot3(result);
					break;
				case LOOP:
					result = subdivisionLoop(result);
					break;
				case DENG_MA:
					result = subdivisionDengMa(result);
					break;
				default:
					result = subdivisionRoot3(result);
					break;
			}
		}
		return result;
	}
	
	public void setSubdivisionMethod(SubdivisionMethod method){
		this.method = method;
	}
	
	public SubdivisionMethod getSubdivisionMethod(){
		return this.method;
	}

	private Shape3D subdivisionDengMa(Shape3D shape) {
		Shape3D result = null;
		return result;
	}

	private Shape3D subdivisionLoop(Shape3D shape) {
		Shape3D result = null;
		return result;
	}

	private Shape3D subdivisionRoot3(Shape3D shape) {
		Shape3D result = null;
		List<Point3d> middlePoints = findMiddlePoints(shape);
		TriangleArray shapeGeometry = (TriangleArray)shape.getGeometry();
		TriangleArray intermidiateGeometry = new TriangleArray((shapeGeometry.getVertexCount())*3,TriangleArray.COORDINATES + TriangleArray.COLOR_3);
		fillIntermidiateGeometry(intermidiateGeometry,shapeGeometry,middlePoints);
		TriangleArray newGeometry = smoothShape(intermidiateGeometry, middlePoints);
		Appearance app = new Appearance();
		GeometryCollorer.colorGeometry(newGeometry);
		result = new Shape3D(newGeometry, app);
		return result;
	}

	private void fillIntermidiateGeometry(TriangleArray intermidiateGeometry,
			TriangleArray shapeGeometry, List<Point3d> middlePoints) {
		int pointCount = shapeGeometry.getVertexCount();
		int midI = 0;
		int intI = 0;
		for(int i = 0 ; i < pointCount ; i += 3){
			Point3d p1 = new Point3d(0.0, 0.0, 0.0);
			Point3d p2 = new Point3d(0.0, 0.0, 0.0);
			Point3d p3 = new Point3d(0.0, 0.0, 0.0);
			Point3d midP = middlePoints.get(midI);
			shapeGeometry.getCoordinate(i, p1);
			shapeGeometry.getCoordinate(i+1, p2);
			shapeGeometry.getCoordinate(i+2, p3);
			intermidiateGeometry.setCoordinate(intI, p1);
			intermidiateGeometry.setCoordinate(intI+1, p2);
			intermidiateGeometry.setCoordinate(intI+2, midP);
			intermidiateGeometry.setCoordinate(intI+3, p2);
			intermidiateGeometry.setCoordinate(intI+4, p3);
			intermidiateGeometry.setCoordinate(intI+5, midP);
			intermidiateGeometry.setCoordinate(intI+6, p1);
			intermidiateGeometry.setCoordinate(intI+7, midP);
			intermidiateGeometry.setCoordinate(intI+8, p3);
			
			midI++;
			intI += 9;
		}
	}

	private List<Point3d> findMiddlePoints(Shape3D shape) {
		List<Point3d> middlePoints = new LinkedList<Point3d>();
		TriangleArray shapeGeometry = (TriangleArray)shape.getGeometry();
		for(int i = 0 ; i < shapeGeometry.getVertexCount() ; i += 3){
			Point3d middlePoint = new Point3d(0.0, 0.0, 0.0);
			Point3d point1 = new Point3d(0.0, 0.0, 0.0);;
			Point3d point2 = new Point3d(0.0, 0.0, 0.0);;
			Point3d point3 = new Point3d(0.0, 0.0, 0.0);;
			shapeGeometry.getCoordinate(i, point1);
			shapeGeometry.getCoordinate(i+1, point2);
			shapeGeometry.getCoordinate(i+2, point3);
			middlePoint.x = point1.x + point2.x + point3.x;
			middlePoint.y = point1.y + point2.y + point3.y;
			middlePoint.z = point1.z + point2.z + point3.z;
			middlePoint.x = middlePoint.x / 3.0;
			middlePoint.y = middlePoint.y / 3.0;
			middlePoint.z = middlePoint.z / 3.0;
			middlePoints.add(middlePoint);
		}
		return middlePoints;
	}
	
	private TriangleArray smoothShape(TriangleArray intermidiateGeometry, List<Point3d> middlePoints) {
		
		int pointCount = intermidiateGeometry.getVertexCount();
		TriangleArray newGeometry = new TriangleArray(pointCount, TriangleArray.COORDINATES + TriangleArray.COLOR_3);
		
		for(int i = 0; i < pointCount; i++) {
			
			Point3d vertex = new Point3d(0.0, 0.0, 0.0);
//			Point3d newVertex = new Point3d(0.0, 0.0, 0.0);
			List<Point3d> neighbours = new LinkedList<Point3d>();
			intermidiateGeometry.getCoordinate(i, vertex);
			
			if(!isMiddlePoint(vertex, middlePoints)){
				neighbours = findVertexNeighbours(vertex, intermidiateGeometry);
				vertex = smoothVertex(vertex, neighbours);
			}
//			System.out.println(vertex.x + ", " + vertex.y + ", " + vertex.z);
			newGeometry.setCoordinate(i, vertex);
//			System.out.println(newVertex.toString());
		}
		
		return newGeometry;
	}
	
	private boolean isMiddlePoint(Point3d vertex, List<Point3d> middlePoints) {
		for(Point3d p : middlePoints){
			if(p.equals(vertex))
				return true;
		}
		return false;
	}

	private Point3d smoothVertex(Point3d vertex, List<Point3d> neighbours) {
		
		Point3d newVertex = new Point3d(0.0, 0.0, 0.0);
		double beta;
		double[] sum = new double[3];
		int nSize = neighbours.size();
				
		beta = (double) ((4 - 2 * Math.cos(2 * Math.PI / nSize)) / (9 * nSize));
		newVertex.x = (1 - nSize * beta) * vertex.x;
		newVertex.y = (1 - nSize * beta) * vertex.y;
		newVertex.z = (1 - nSize * beta) * vertex.z;
		
		for(int i = 0; i < 3; i++) {
			sum[i] = 0;
		}
		
		for(int i = 0; i < nSize; i++) {
			sum[0] += neighbours.get(i).x;
			sum[1] += neighbours.get(i).y;
			sum[2] += neighbours.get(i).z;
		}
		
		newVertex.x += beta*sum[0];
		newVertex.y += beta*sum[1];
		newVertex.z += beta*sum[2];
		
		return newVertex;
	}
	
	private List<Point3d> findVertexNeighbours(Point3d vertex, TriangleArray intermidiateGeometry) {
		
		List<Point3d> neighbours = new LinkedList<Point3d>();
		int pointCount = intermidiateGeometry.getVertexCount();
		
		for(int i = 0; i < pointCount; i += 3) {
			if(hasVertex(i, vertex, intermidiateGeometry)) {
				for(int j = i; j < i+3; j++) {
					Point3d possiblePoint = new Point3d(0.0, 0.0, 0.0);
					intermidiateGeometry.getCoordinate(j, possiblePoint);
					if(!possiblePoint.equals(vertex) && !neighbours.contains(possiblePoint)) {
						neighbours.add(possiblePoint);
					}
				}
			}
		}
				
		return neighbours;
	}
	
	private Boolean hasVertex(int polygon, Point3d vertex, TriangleArray intermidiateGeometry) {
		
		for(int i = polygon; i < polygon+3; i++) {
			Point3d possibleVertex = new Point3d(0.0, 0.0, 0.0);
			intermidiateGeometry.getCoordinate(i, possibleVertex);
			if(vertex.equals(possibleVertex)) {
				return true;
			}
		}
		
		return false;
	}
}
