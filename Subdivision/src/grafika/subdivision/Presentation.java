package grafika.subdivision;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.GraphicsConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.Locale;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.View;
import javax.media.j3d.ViewPlatform;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.applet.MainFrame;
import com.sun.j3d.utils.geometry.ColorCube;
import com.sun.j3d.utils.universe.SimpleUniverse;

public class Presentation extends Applet implements ActionListener, KeyListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6548851190690756128L;
	private SimpleUniverse universe = null;
	private BranchGroup sceneGroup = null;
	private List<TransformGroup> sceneTransGroups = null;

	public static void main( String[] args ) {
		Presentation presentation = new Presentation();
		presentation.initialize();
		Subdiviser sub = new Subdiviser(SubdivisionMethod.ROOT_3);
		Shape3D s1 = presentation.getShapes().get(0);
		for(int i = 0 ; i < 6 ; i++){
			s1 = sub.subdivision(s1, 1);
			presentation.addShapeToSceneGraph(s1, new Point3d(((double)i + 1.0) * 3.0, 0.0, -3.0));
		}
		ShapeFactory factory = new ShapeFactory();
		s1 = factory.getSquare();
		presentation.addShapeToSceneGraph(s1, new Point3d(0.0, -3.0, -3.0));
		for(int i = 0 ; i < 6 ; i++){
			s1 = sub.subdivision(s1, 1);
			presentation.addShapeToSceneGraph(s1, new Point3d(((double)i + 1.0) * 3.0, -3.0, -3.0));
		}
		s1 = factory.getTetrahedron();
		presentation.addShapeToSceneGraph(s1, new Point3d(0.0, -6.0, -3.0));
		for(int i = 0 ; i < 6 ; i++){
			s1 = sub.subdivision(s1, 1);
			presentation.addShapeToSceneGraph(s1, new Point3d(((double)i + 1.0) * 3.0, -6.0, -3.0));
		}
		s1 = factory.getCube();
		presentation.addShapeToSceneGraph(s1, new Point3d(0.0, -9.0, -3.0));
		for(int i = 0 ; i < 6 ; i++){
			s1 = sub.subdivision(s1, 1);
			presentation.addShapeToSceneGraph(s1, new Point3d(((double)i + 1.0) * 3.0, -9.0, -3.0));
		}
		presentation.sceneGroup.compile();
		presentation.universe.addBranchGraph(presentation.sceneGroup);
		MainFrame frame = new MainFrame(presentation, 1024, 1024);
	}

	private void initialize() {
		setLayout(new BorderLayout());
        GraphicsConfiguration config =
            SimpleUniverse.getPreferredConfiguration();
        Canvas3D canvas = new Canvas3D(config);
	    add("Center", canvas);
	    canvas.addKeyListener(this);
	    this.setUpSceneGraph();
	    this.setUpLights();
        this.universe = new SimpleUniverse(canvas);
        this.universe.getViewingPlatform().setNominalViewingTransform();
        this.universe.addBranchGraph(this.sceneGroup);
	}
	
	private void setUpLights(){
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
		Color3f light1Color = new Color3f(1.0f, 0.0f, 0.2f);
	    Vector3f light1Direction  = new Vector3f(4.0f, -7.0f, -12.0f);
	    DirectionalLight light1
	      = new DirectionalLight(light1Color, light1Direction);
	    light1.setInfluencingBounds(bounds);
	    this.sceneGroup.addChild(light1);
	    Color3f ambientColor = new Color3f(1.0f, 1.0f, 1.0f);
	    AmbientLight ambientLightNode = new AmbientLight(ambientColor);
	    ambientLightNode.setInfluencingBounds(bounds);
	    this.sceneGroup.addChild(ambientLightNode);

	}
	
	private void setUpSceneGraph(){
		this.sceneGroup = new BranchGroup();
		this.sceneGroup.setCapability(BranchGroup.ALLOW_DETACH);
		this.sceneGroup.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
		this.sceneGroup.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
		this.sceneGroup.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
		this.sceneGroup.setCapability(BranchGroup.ALLOW_BOUNDS_READ);
		this.sceneGroup.setCapability(BranchGroup.ALLOW_BOUNDS_WRITE);
		this.sceneTransGroups = new LinkedList<TransformGroup>();
		TransformGroup tg = new TransformGroup();
		this.sceneTransGroups.add(tg);
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
	    Transform3D trans = new Transform3D();    
	    trans.setTranslation(new Vector3d(0.0,0.0,-3.0));
	    tg.setTransform(trans);
	    ShapeFactory f = new ShapeFactory();
	    Shape3D s = f.getTriangle();
	    tg.addChild(s);
	    this.sceneGroup.addChild(tg);
	}
	
	public void addShapeToSceneGraph(Shape3D shape, Point3d location){
		this.sceneGroup.detach();
		TransformGroup newGroup = new TransformGroup();
		this.sceneTransGroups.add(newGroup);
		newGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Transform3D newTrans = new Transform3D();
		newTrans.setTranslation(new Vector3d(location.x, location.y, location.z));
		newGroup.setTransform(newTrans);
		newGroup.addChild(shape);
		this.sceneGroup.addChild(newGroup);
	}
	
	public List<Shape3D> getShapes(){
		List<Shape3D> shapes = new LinkedList<Shape3D>();
		for(TransformGroup tg : this.sceneTransGroups){
			Enumeration<Object> e = tg.getAllChildren();
			while(e.hasMoreElements()){
				Object o = e.nextElement();
				if(o instanceof Shape3D){
					shapes.add((Shape3D) o);
				}
			}
		}
		return shapes;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		double xDiff = 0.0;
		double yDiff = 0.0;
		double zDiff = 0.0;
		if (e.getKeyChar()=='a'){
			xDiff = -0.5;
		}
		if (e.getKeyChar()=='w'){
			yDiff = 0.5;
		}
		if (e.getKeyChar()=='d'){
			xDiff = 0.5;
		}
		if (e.getKeyChar()=='s'){
			yDiff = -0.5;
		}
		if (e.getKeyChar()=='q'){
			zDiff = 0.5;
		}
		if (e.getKeyChar()=='e'){
			zDiff = -0.5;
		}
//		if (e.getKeyChar()=='j'){
//			this.Yrot -= Math.PI/6;
//		}
//		if (e.getKeyChar()=='i'){
//			this.Xrot -= Math.PI/6;
//		}
//		if (e.getKeyChar()=='l'){
//			this.Yrot += Math.PI/6;
//		}
//		if (e.getKeyChar()=='k'){
//			this.Xrot += Math.PI/6;
//		}
		for(TransformGroup tg : this.sceneTransGroups){
			Transform3D tmp = new Transform3D();
			tg.getTransform(tmp);
			Vector3d tmpVec = new Vector3d(0.0,0.0,0.0);
			tmp.get(tmpVec);
			tmpVec.x += xDiff;
			tmpVec.y += yDiff;
			tmpVec.z += zDiff;
			tmp.setTranslation(tmpVec);
			tg.setTransform(tmp);
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
	}
}
